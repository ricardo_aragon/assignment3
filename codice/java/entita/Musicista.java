package entita;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="MUSICISTA")
public class Musicista {

	@Id
	@Column(name="NOME")
	private String nome;
	
	@Column(name="STRUMENTO")
	private String strumento;
	
	@ManyToOne(fetch =FetchType.LAZY)
	@JoinColumn(name="BAND")
	private Band band;
	
	
	private Musicista capo_band;
	
	private List<Musicista> dipendenti;
	
	public Musicista() {}

	public Musicista(String nome, String strumento, Band band, Musicista capo_band, List<Musicista> dipendenti) {
		this.nome = nome;
		this.strumento = strumento;
		this.band = band;
		this.capo_band = capo_band;
		this.dipendenti = dipendenti;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getStrumento() {
		return strumento;
	}

	public void setStrumento(String strumento) {
		this.strumento = strumento;
	}

	public Band getBand() {
		return band;
	}

	public void setBand(Band band) {
		this.band = band;
	}

	public Musicista getCapo_band() {
		return capo_band;
	}

	public void setCapo_band(Musicista capo_band) {
		this.capo_band = capo_band;
	}

	public List<Musicista> getDipendenti() {
		return dipendenti;
	}

	public void setDipendenti(List<Musicista> dipendenti) {
		this.dipendenti = dipendenti;
	}
	

	
	
	
}
