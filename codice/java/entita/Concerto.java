package entita;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="CONCERTO")
public class Concerto {
	
	@Id
	@Column(name="CONCERTO_ID")
	private String nome;
	
	@Column(name="UBICAZIONE")
	private String ubicazione;
	
	@Column(name="ORA")
	private LocalTime ora;
	
	@Column(name="COSTO")
	private int costo;
	
	@Column(name="DATA")
	private LocalDate data;
	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(
		name= "GRUPPO",
		joinColumns= {@JoinColumn(name ="CONCERTO_ID")},
		inverseJoinColumns= {@JoinColumn(name="BAND_ID")}
	)
	private List<Band> gruppo;
	
	public Concerto() {}

	public Concerto(String nome, String ubicazione, LocalTime ora, int costo, LocalDate data, List<Band> gruppo) {
		this.nome = nome;
		this.ubicazione = ubicazione;
		this.ora = ora;
		this.costo = costo;
		this.data = data;
		this.gruppo = gruppo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUbicazione() {
		return ubicazione;
	}

	public void setUbicazione(String ubicazione) {
		this.ubicazione = ubicazione;
	}

	public LocalTime getOra() {
		return ora;
	}

	public void setOra(LocalTime ora) {
		this.ora = ora;
	}

	public int getCosto() {
		return costo;
	}

	public void setCosto(int costo) {
		this.costo = costo;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public List<Band> getGruppo() {
		return gruppo;
	}

	public void setGruppo(List<Band> gruppo) {
		this.gruppo = gruppo;
	}

	/*public void addGruppo(Band band) {
		if(!gruppo.contains(band)) {
			gruppo.add(band);
			band
		}
	}
	
	public void removeGruppo(Band band) {
		if(gruppo.contains(band)) {
			gruppo.remove(band);
			//per eliminare completamente la relazione con concerto
			band
			
		}
	}*/
	@Override
	public String toString() {
		return "Concerto [nome=" + nome + ", ubicazione=" + ubicazione + ", ora=" + ora + ", costo=" + costo + ", data="
				+ data + ", numero_gruppi=" + gruppo.size() + "]";
	}

}
