package entita;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
 
@Entity
@Table(name="Brano")
public class Brano {

	@Id
	@Column(name="nome")
	private String nome;
	
	@Column(name="durata")
	private int durata;
	
	@Column(name="artista")
	private String artista;
	
	@ManyToOne(fetch =FetchType.LAZY)
	@JoinColumn(name="SCALETTA_ID")
	private Scaletta scaletta;
	
	public Brano() {}
	public Brano(String nome, int durata, String artista) {
		this.nome=nome;
		this.durata=durata;
		this.artista=artista;
		this.scaletta=null;
	}


	public Brano(String nome, int durata, String artista, Scaletta scaletta) {
		this.nome = nome;
		this.durata = durata;
		this.artista = artista;
		this.scaletta = scaletta;
	}
	public void setDurata(int durata) {
		this.durata = durata;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}

	public int getDurata() {
		return durata;
	}

	public String getArtista() {
		return artista;
	}

	public Scaletta getScaletta() {
		return scaletta;
	}

	public void setScaletta(Scaletta scaletta) {
		this.scaletta = scaletta;
	}

	@Override
	public String toString() {
		return "Brano [nome=" + nome + ", durata=" + durata + ", artista=" + artista + ", scaletta=" + scaletta.getNome() + "]";
	}
	
	

	

}