package entita;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name= "BAND")
public class Band {
	@Id
	@Column(name="BAND_ID")
	private String nome;
	
	@Column(name="NUMERO_INTEGRANTI")
	private int numero_integranti;
	
	@Column(name="GENERE")
	private String genere;
	
	@Column(name="COSTO")
	private int costo;
	
	@ManyToMany(mappedBy= "gruppo")
	private List<Concerto> concerto;
	
	@OneToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="SCALETTA")
	private Scaletta scaletta;
	
	@OneToMany(mappedBy ="band" , cascade=CascadeType.ALL,orphanRemoval=true)
	private List<Musicista> musicista;
	

	public Band() {}
	public Band(String nome, int numero_integranti, String genere, int costo,
			Scaletta scaletta) {
		this.nome = nome;
		this.numero_integranti = numero_integranti;
		this.genere = genere;
		this.costo = costo;
		this.concerto = new ArrayList<Concerto>(5);
		this.scaletta = null;
		this.musicista=new ArrayList<Musicista>(numero_integranti);
	}
	public Band(String nome, int numero_integranti, String genere, int costo, List<Concerto> concerto,
			Scaletta scaletta,List<Musicista> musicista) {
		this.nome = nome;
		this.numero_integranti = numero_integranti;
		this.genere = genere;
		this.costo = costo;
		this.concerto = concerto;
		this.scaletta = scaletta;
		this.musicista=musicista;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getNumero_integranti() {
		return numero_integranti;
	}

	public void setNumero_integranti(int numero_integranti) {
		this.numero_integranti = numero_integranti;
	}

	public String getGenere() {
		return genere;
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}

	public int getCosto() {
		return costo;
	}

	public void setCosto(int costo) {
		this.costo = costo;
	}

	public List<Concerto> getConcerto() {
		return concerto;
	}

	public void setConcerto(List<Concerto> concerto) {
		this.concerto = concerto;
	}

	public Scaletta getScaletta() {
		return scaletta;
	}

	public void setScaletta(Scaletta scaletta) {
		this.scaletta = scaletta;
	}
	public List<Musicista> getMusicista() {
		return musicista;
	}
	public void setMusicista(List<Musicista> musicista) {
		this.musicista = musicista;
	}
	public void addMusicista(Musicista strumentista) {
		if(!musicista.contains(strumentista)) {
			musicista.add(strumentista);
			strumentista.setBand(this);
		}
	}
	
	public void removeMusicista(Musicista strumentista) {
		if(musicista.contains(strumentista)) {
			musicista.remove(strumentista);
			//per eliminare completamente la relazione con scaletta
			strumentista.setBand(null);
			
		}
	}
	@Override
	public String toString() {
		return "Band [nome=" + nome + ", numero_integranti=" + numero_integranti + ", genere=" + genere + ", costo="
				+ costo + ", concerto=" + concerto.toString() + ", scaletta=" + scaletta.toString() + ", muisicista=" + musicista.toString() + "]";
	}
	
	

	
	

}
