# Assignment3

# Project Title

Il progetto comprende la gestione della persistenza di un insieme di entità 
scritte in Java e mappate attraverso in un database relazionale implementato
con H2, la mappatura avviene con JPA attraverso l'utilizzo di Hibernate e la 
struttura del progetto e le diverse dipendenze sono gestite con Maven.

## Getting Started


```
git clone https://gitlab.com/ricardoanibalmatamorosaragon/assignment3
```

### Prerequisites


```
Java Version 8 Update 191

```

```
Install Eclipse

```


```
Install H2

```
### Installing


```
Importare il progetto in Eclipse

```

```
Configurare i parametri del DB in H2

```

## Built With

* [Eclipse](https://www.eclipse.org/downloads/) - Ambiente di sviluppo
* [Maven](https://maven.apache.org/) - Dependency Management
* [H2](http://www.h2database.com/html/main.html) - DB relazionale
* [Hibernate](http://hibernate.org/orm/releases/) - Framework JPA
* [Java](https://www.java.com/it/download/)- linguaggio di programmazione


## Authors

* **Ricardo Aragon** - *Initial work* - [Assignment3](https://gitlab.com/ricardoanibalmatamorosaragon/assignment3)


